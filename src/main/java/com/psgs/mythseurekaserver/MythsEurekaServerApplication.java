package com.psgs.mythseurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class MythsEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MythsEurekaServerApplication.class, args);
	}

}
